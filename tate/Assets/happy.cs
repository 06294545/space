﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class happy : MonoBehaviour
{
    public GameObject happyPrefab;
    float span = 1.2f;
    float delta = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        delta += Time.deltaTime;
        Debug.Log(delta);
        if (delta > span)
        {
            delta = 0;  //１秒後にリセット
            GameObject go = Instantiate(happyPrefab);
            int px = Random.Range(-6, 9);  //矢がランダムに降ってくる
            go.transform.position = new Vector3(8, px, 0);
        }
    }
}
